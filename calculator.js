function generateButtons()
{  
    let table = document.getElementById("tableBody");
    for(rowIndex = 2; rowIndex < 5; rowIndex++)
    {
        relativeRowIndex = rowIndex - 2;
        let row = table.children[rowIndex];
        for(colIndex = 2; colIndex >= 0; colIndex--)
        {
            let buttonNumber = 3 * relativeRowIndex + colIndex + 1;

            let newCell = row.insertCell(0);        
            newCell.innerHTML = "<button>" + buttonNumber + "</button>";
            newCell.children[0].classList.add("numberButton");
            newCell.children[0].setAttribute("onclick", "numberClicked(this)");
        }
    }
}

let lastResult = 0;
let newNumber = true;
let lastOperatorPerformed = true;
let outputControl = document.getElementById("resultLabel");
let currentOperator;

function numberClicked(button)
{
    let number = button.innerHTML;
    if(newNumber || outputControl.innerHTML == "0")
    {
        newNumber = false;
        outputControl.innerHTML = number;
    }
    else
    {
        outputControl.innerHTML += number;
    }
}

function changeSign()
{
    if(outputControl.innerHTML[0] == "-")
    {
        outputControl.innerHTML =  outputControl.innerHTML.replace("-", "");
    }
    else
    {
        outputControl.innerHTML =  "-" + outputControl.innerHTML;
    }
}

function clearOutput()
{
    newNumber = true;
    lastOperatorPerformed = true;
    outputControl.innerHTML = "0";
}
function divideClicked()
{
    if(outputControl.innerHTML == "0")
    {
        return;
    }

    if(!lastOperatorPerformed)
    {
        equalsClicked();
        lastOperatorPerformed = false;
    }

    currentOperator = (x, y) => x / y;
    operatorClicked();
}
function multiplyClicked()
{
    if(!lastOperatorPerformed)
    {
        equalsClicked();
        lastOperatorPerformed = false;
    }
    
    currentOperator = (x, y) => x * y;
    operatorClicked();
}
function minusClicked()
{
    if(!lastOperatorPerformed)
    {
        equalsClicked();
        lastOperatorPerformed = false;
    }

    currentOperator = (x, y) => x - y;
    operatorClicked();
}
function plusClicked()
{
    if(!lastOperatorPerformed)
    {
        equalsClicked();
        lastOperatorPerformed = false;
    }

    currentOperator = (x, y) => Number(x) + Number(y);
    operatorClicked();
}
function equalsClicked()
{
    if(newNumber)
    {
        return;
    }
    
    outputControl.innerHTML = currentOperator(lastResult, outputControl.innerHTML);
    lastResult = outputControl.innerHTML;
    newNumber = true;
    lastOperatorPerformed = true;
}
function dotClicked()
{
    if(!canPlaceDot())
    {
        return;
    }
    outputControl.innerHTML += ".";
}

function canPlaceDot()
{
    return outputControl.innerHTML && !outputControl.innerHTML.includes(".");
}

function operatorClicked()
{
    lastResult = outputControl.innerHTML;
    newNumber = true;
    lastOperatorPerformed = false;
}
